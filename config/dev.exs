use Mix.Config

# For development, we disable any cache and enable
# debugging and code reloading.
#
# The watchers configuration can be used to run external
# watchers to your application. For example, we use it
# with brunch.io to recompile .js and .css sources.
config :nimbus2000, Nimbus2000.Endpoint,
  url: [scheme: "https", host: "localhost:4443", port: 443],
  http: [port: 4000],
  force_ssl: [rewrite_on: [:x_forwarded_proto]],
  https: [port: 4443,
          otp_app: :nimbus2000,
          keyfile: "priv/keys/localhost.key",
          certfile: "priv/keys/localhost.cert"],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: [node: ["node_modules/brunch/bin/brunch", "watch", "--stdin",
                    cd: Path.expand("../", __DIR__)]]


# Watch static and templates for browser reloading.
config :nimbus2000, Nimbus2000.Endpoint,
  live_reload: [
    patterns: [
      ~r{priv/static/.*(js|css|png|jpeg|jpg|gif|svg)$},
      ~r{priv/gettext/.*(po)$},
      ~r{web/views/.*(ex)$},
      ~r{web/templates/.*(eex)$}
    ]
  ]

# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20

# Configure your database
config :nimbus2000, Nimbus2000.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "nimbus2000",
  password: "Film_45!",
  database: "nimbus2000_dev",
  hostname: "localhost",
  port: "5433",
  pool_size: 10
