use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :nimbus2000, Nimbus2000.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :nimbus2000, Nimbus2000.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "nimbus2000",
  password: "Film_45!",
  database: "nimbus2000_test",
  hostname: "localhost",
  port: "5433",
  pool: Ecto.Adapters.SQL.Sandbox
