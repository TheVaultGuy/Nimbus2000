defmodule Nimbus2000.Repo.Migrations.CreateAssets do
  use Ecto.Migration

  def change do
    create table(:assets, primary_key: false) do
      add :asset_tag, :string, primary_key: true
      add :brand, :string, null: false
      add :description, :string, default: "", null: false
      add :serial, :string, null: false
      add :notes, :string, default: "", null: false
      add :active, :boolean, default: true, null: false

      timestamps()
    end

  end
end
