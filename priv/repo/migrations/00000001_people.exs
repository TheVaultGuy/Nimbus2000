defmodule Nimbus2000.Repo.Migrations.CreatePerson do
  use Ecto.Migration

  def change do
    create table(:people) do
      add :first_name, :string, null: false
      add :last_name, :string, null: false
      add :company, :string, default: "Film 45", null: true
      add :bio, :string, default: "", null: false
      add :notes, :string, default: "", null: false
      add :profile_pic_url, :string, default: "http://www.avatarpro.biz/avatar/film45?s=256", null: true

      timestamps()
    end

    create index(:people, [:first_name, :last_name])
  end
end
