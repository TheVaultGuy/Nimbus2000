defmodule Nimbus2000.Repo.Migrations.CreateProject do
  use Ecto.Migration

  def up do
    execute ("create type projectStatus as enum ('in development', 'in pre-production', 'in production', 'in post-production', 'inactive')")

    create table(:projects, primary_key: false) do
      add :number, :integer, null: false, primary_key: true
      add :code, :string, null: false
      add :title, :string, null: false
      add :company, :string, default: "Film 45", null: false
      add :about, :string, default: "", null: false
      add :status, :projectStatus, default: "in development", null: false

      timestamps
    end

    create unique_index(:projects, [:number])
    create index(:projects, [:code])
    create index(:projects, [:status])
  end

  def down do
    drop table(:projects)
    execute ("drop type projectStatus")
  end

end
