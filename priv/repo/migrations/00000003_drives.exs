defmodule Nimbus2000.Repo.Migrations.CreateDrive do
  use Ecto.Migration

  def change do
    create table(:drives, primary_key: false) do
      add :asset_id, references(:assets, column: :asset_tag, type: :string, on_delete: :delete_all), primary_key: true
      add :description, :string, default: "", null: false
      add :name, :string, null: true
      add :short_name, :string, null: true
      add :capacity, :bigint, default: 1000000000000, null: false
      add :transfer, :boolean, default: false, null: false

      timestamps()
    end

    create unique_index(:drives, [:name, :short_name], where: "transfer = false", name: :drive_name_short_name_index)
  end
end
