defmodule Nimbus2000.Repo.Migrations.CreateMedium do
  use Ecto.Migration

  def up do
    execute ("create type mediaType as enum ('VID', 'AUD', 'ELE', 'FEL', 'PIC', 'GFX', 'MAS', 'MUS', 'DOC')")
    create table(:media) do
      add :description, :string, default: "", null: false
      add :notes, :string, default: "", null: false
      add :size, :bigint, default: 0, null: false
      add :project_number, references(:projects, column: :number, type: :integer, on_delete: :delete_all)
      add :offloadDate, :datetime, default: fragment("now()"), null: false
      add :shootDate, :date, default: fragment("now()"), null: false
      add :type, :mediaType
      add :card_letter, :string, default: "", null: false
      add :card_number, :integer, default: 1, null: true
      add :report_url, :string, null: true

      timestamps()
    end

    create unique_index(:media, [:project_number, :shootDate, :type, :card_letter, :card_number, :notes])
  end

  def down do
    drop table(:media)
    execute ("drop type mediaType")
  end
end
