defmodule Nimbus2000.Repo.Migrations.CreatePurchase do
  use Ecto.Migration

  def up do
    execute ("create type paymentType as enum ('Credit Card', 'Cash', 'Check', 'Direct Deposit', 'Third Party Payment Service')")
    create table(:purchases) do
      add :project_number, references(:projects, column: :number, type: :integer, on_delete: :delete_all)
      add :date, :date, default: fragment("now()"), null: false
      add :vendor, :string, null: false
      add :description, :string, null: false
      add :notes, :string, null: false
      add :amount, :bigint, default: 0, null: false
      add :payment, :paymentType, null: false
      add :report_url, :string, default: "", null: false

      timestamps()
    end
  end

  def down do
    drop table(:purchases)
    execute ("drop type paymentType")
  end
end
