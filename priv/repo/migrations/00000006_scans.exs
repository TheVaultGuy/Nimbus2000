defmodule Nimbus2000.Repo.Migrations.CreateScan do
  use Ecto.Migration

  def change do
    create table(:scans) do
      add :asset_tag, references(:assets, column: :asset_tag, type: :string, on_delete: :delete_all), null: false
      add :project_number, references(:projects, column: :number, type: :integer, on_delete: :delete_all), default: 10000, null: false
      add :location, :string, null: false
      add :owner, references(:people, column: :id, on_delete: :delete_all)
      add :notes, :string, default: "", null: false
      add :runslip_url, :string, default: "", null: false

      timestamps
    end

    create index(:scans, [:location])
    create index(:scans, [:owner])
    create index(:scans, [:asset_tag])
    create index(:scans, [:project_number])
    create index(:scans, [:inserted_at])
  end
end
