defmodule Nimbus2000.Repo.Migrations.CreateTranscription do
  use Ecto.Migration

  def change do
    create table(:transcriptions) do
      add :name, :string, null: false
      add :description, :string, default: "", null: false
      add :company, :string, default: "BAM", null: false
      add :date_out, :date, null: false
      add :date_in, :date, null: true
      add :notes, :string, default: "", null: false
      add :raw_url, :string, null: true
      add :formatted_url, :string, null: true
      add :active, :boolean, default: true, null: false

      timestamps()
    end

  end
end
