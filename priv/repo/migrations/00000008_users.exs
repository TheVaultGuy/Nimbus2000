defmodule Nimbus2000.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    create table(:users, primary_key: false) do
      add :id, references(:people, column: :id, on_delete: :delete_all), primary_key: true
      add :email, :string, null: false
      add :password_hash, :string, null: false
      add :admin, :boolean, default: false, null: false
      add :active, :boolean, default: true, null: false

      timestamps
    end

    create unique_index(:users, [:email])
  end
end
