defmodule Nimbus2000.Repo.Migrations.JoinPersonProject do
  use Ecto.Migration

  def change do
    create table(:people_projects, primary_key: false) do
      add :role, :string, default: "", null: false
      add :write, :boolean, default: false, null: false
      add :project_number, references(:projects, column: :number, on_delete: :delete_all), primary_key: true
      add :person_id, references(:people, on_delete: :delete_all), primary_key: true
    end
  end
end
