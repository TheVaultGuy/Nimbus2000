defmodule Nimbus2000.Repo.Migrations.JoinAssetPurchase do
  use Ecto.Migration

  def change do
      create table(:asset_purchase, primary_key: false) do
        add :asset_id, references(:assets, column: :asset_tag, type: :string, on_delete: :delete_all), primary_key: true
        add :purchase_id, references(:purchases, on_delete: :delete_all), primary_key: true
        add :capex, :boolean, default: false, null: false
      end
  end
end
