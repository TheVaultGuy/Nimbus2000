defmodule Nimbus2000.Repo.Migrations.JoinDriveMedium do
  use Ecto.Migration

  def change do
      create table(:drives_media, primary_key: false) do
        add :drive_id, references(:drives, column: :asset_id, type: :string, on_delete: :delete_all), primary_key: true
        add :media_id, references(:media, on_delete: :delete_all), primary_key: true
        add :folderpath, :string, default: "/", primary_key: true #This is the slave name
      end
  end
end
