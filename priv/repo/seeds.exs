# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Nimbus2000.Repo.insert!(%Nimbus2000.SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
Nimbus2000.Repo.insert!(%Nimbus2000.HardDrive{asset_tag: 1204, show: "C17", backup_letter: "A", backup_number: 1})
