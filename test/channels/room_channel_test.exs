defmodule Nimbus2000.RoomChannelTest do
  use Nimbus2000.ChannelCase

  alias Nimbus2000.RoomChannel

  setup do
    {:ok, _, socket} =
      socket("user_id", %{some: :assign})
      |> subscribe_and_join(RoomChannel, "room:lobby")

    {:ok, socket: socket}
  end

  test "ping replies with status ok", %{socket: socket} do
    ref = push socket, "ping", %{"hello" => "there"}
    assert_reply ref, :ok, %{"hello" => "there"}
  end

  test "shout broadcasts to room:lobby", %{socket: socket} do
    push socket, "shout", %{body: "allll"}
    assert_broadcast "shout", %{body: "allll"}
  end

  test "broadcasts are pushed to the client", %{socket: socket} do
    broadcast_from! socket, "shout", %{body: "data"}
    assert_push "shout", %{body: "data"}
  end
end
