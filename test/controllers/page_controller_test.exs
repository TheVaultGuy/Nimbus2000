defmodule Nimbus2000.PageControllerTest do
  use Nimbus2000.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "F45 - Nimbus 2000"
  end
end
