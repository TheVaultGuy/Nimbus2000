defmodule Nimbus2000.HardDriveTest do
  use Nimbus2000.ModelCase

  alias Nimbus2000.HardDrive

  @valid_attrs %{asset: "F450112", description: "some content", letter: "A", number: 42, serial: "some serial", capacity: 115456, free_space: 564862}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = HardDrive.changeset(%HardDrive{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = HardDrive.changeset(%HardDrive{}, @invalid_attrs)
    refute changeset.valid?
  end
end
