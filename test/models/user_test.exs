defmodule Nimbus2000.UserTest do
  use Nimbus2000.ModelCase

  alias Nimbus2000.User

  @valid_attrs %{admin: true, email: "some content", first_name: "some content", last_name: "some content", staff: true, password: "password", password_confirmation: "password"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = User.changeset(%User{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = User.changeset(%User{}, @invalid_attrs)
    refute changeset.valid?
  end
end
