import Ecto.Query

defmodule Nimbus2000.RoomChannel do
  use Nimbus2000.Web, :channel

  intercept ["shout", "create", "read", "update", "delete", "ack"]

  def join("room:lobby", %{"token" => token}, socket) do
    # 1 day = 86400 seconds
    case Phoenix.Token.verify(socket, "nimbus2000TokenSalt", token, max_age: 86400) do
      {:ok, user_id} ->
        # user = Passport.Config.repo.get!(Passport.Config.resource, user_id)
        # person = Nimbus2000.Repo.get!(Nimbus2000.Person, user_id)
        IO.inspect user_id
        query = from user in Nimbus2000.User,
          preload: [:projects, :person]
        user = Nimbus2000.Repo.get(query, user_id)
        projects = for project <- user.projects do project.number end
        payload = %{first_name: user.person.first_name, last_name: user.person.last_name, email: user.email, bio: user.person.bio, person_id: user.person.id, projects: projects, admin: user.admin, active: user.active}
        socket = assign(socket, :current_user, user)
        {:ok, payload, socket}
      {:error, errorMessage} ->
        {:error, {:error, %{errorMessage: errorMessage}}, socket}
    end
  end

  def handle_info(:after_join, socket) do
    push socket, "system", %{body: "Welcome to Nimbus 2000!"}
    {:noreply, socket}
  end

  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (room:lobby).
  def handle_in("shout", %{"body" => body}, socket) do
    broadcast! socket, "shout", %{body: body}
    IO.puts (["Got a shout: ", body])
    {:noreply, socket}
  end

  def handle_in("read", %{"table" => table, "preload" => preload, "filters" => filters}, socket) do
    user = socket.assigns[:current_user]
    # user

    # working Create!!!
    # if user != nil do
    #   changeset = Nimbus2000.Project.changeset(%Nimbus2000.Project{}, %{number: 10070, code: "NVK", title: "Untitled Novak Djokovic Series"})
    #   case Nimbus2000.Repo.insert(changeset) do
    #     {:ok, newProj} ->
    #       IO.puts "I Think it Worked!!!!"
    #     {:error, changeset} ->
    #       IO.puts "Damn"
    #   end
    # end
    # query = from Nimbus2000.User
    # users = Nimbus2000.Repo.all(query)
    # users = Enum.map(users, fn v -> %{first_name: v.first_name, last_name: v.last_name} end)
    # payloadOut = %{payload | "body" => users }
    # push socket, "read", payloadOut

    # newProj = Nimbus2000.Project.changeset(%Nimbus2000.Project{}, %{code: "ABC", number: 1000, title: "Test Project", company: "Film 45", about: "About Field...", status: "inactive"})
    # case Nimbus2000.Repo.insert (newProj) do
    #   {:ok, asdf} ->
    #     IO.inspect asdf
    #   {:error, changeset} ->
    #     IO.puts "ERROR"
    #     IO.inspect changeset
    # end

    # try do
    #   #insertProject?(%{code: "DEF", number: 1001, title: "Test Project 2", company: "Film 45", about: "About Field...", status: "inactive"})
    #   project = (first Nimbus2000.Project) |> Nimbus2000.Repo.one |> Repo.preload(:users)
    #   user = socket.assigns.current_user
    #   insertProjectUser?(project.number, user.id)
    #   IO.inspect project
    # rescue
    #   e in RuntimeError -> IO.inspect e
    # end

    payload = case table do
      "projects" ->
        IO.puts "got to projects"
        query = from Nimbus2000.Project
        projects = Nimbus2000.Repo.all(query)
        projects = Enum.map(projects, fn v -> %{code: v.code, title: v.title, company: v.company, about: v.about, status: v.status} end)
        %{body: projects}
      "users" ->
        IO.puts "got to users"
        query = from Nimbus2000.User,
          preload: [:person, :projects]
        users = Nimbus2000.Repo.all(query)
        users = Enum.map(users, fn v -> %{first_name: v.person.first_name, last_name: v.person.last_name, email: v.email, person_id: v.person.id, admin: v.admin, active: v.active, bio: v.person.bio, projects: v.projects} end)
        %{body: users}
      "people" ->
        IO.puts "got to people"
        query = from Nimbus2000.Person,
          preload: [:user, :projects]
        people = Nimbus2000.Repo.all(query)
        people = Enum.map(people, fn v -> %{person_id: v.id, user_id: v.user.id, profile_pic_url: v.profile_pic_url, first_name: v.first_name, last_name: v.last_name, company: v.company, bio: v.bio, notes: v.notes, projects: v.projects} end)
        %{body: people}
      _->
        IO.puts "got to _"
        %{}
    end


    IO.inspect (["Got a message: ", table])
    {:reply, {:ok, payload}, socket}
  end

  def handle_in(in_type, in_payload, in_socket) do
    IO.puts "Message didn't match any handle_in functions :("
    IO.puts in_type
    IO.inspect in_payload
    IO.inspect in_socket
    {:noreply, in_socket}
  end

  def handle_out("shout", %{body: body}, socket) do
    push socket, "shout", %{body: body}
    IO.inspect (["Broadcasting: ", body])
    {:noreply, socket}
  end

  # Add authorization logic here as required.
  defp authorized?(crud, project_number, user) do
    if user.admin do
      true
    else
      false
    end
  end

  defp insertProject?(projectParams) do
    newProject = Nimbus2000.Project.changeset(%Nimbus2000.Project{}, projectParams)
    case Nimbus2000.Repo.insert (newProject) do
      {:ok, _} ->
        true
      {:error, changeset} ->
        IO.puts "ERROR"
        IO.inspect changeset
        false
    end
  end

  defp insertProjectUser?(projectID, userID, write \\ false, role \\ "contributor") do
    newProjectUser = Nimbus2000.Join.Projects_Users.changeset(%Nimbus2000.Join.Projects_Users{}, %{project_id: projectID, user_id: userID, write: write, role: role})
    case Nimbus2000.Repo.insert (newProjectUser) do
      {:ok, _} ->
        true
      {:error, changeset} ->
        IO.puts "ERROR"
        IO.inspect changeset
        false
    end
  end


end
