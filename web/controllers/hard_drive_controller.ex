defmodule Nimbus2000.HardDriveController do
  use Nimbus2000.Web, :controller

  alias Nimbus2000.HardDrive

  # def index(conn, _params) do
  #   drives = Repo.all(HardDrive)
  #   render(conn, "index.json", drives: drives)
  # end
  #
  # def create(conn, %{"hard_drive" => hard_drive_params}) do
  #   changeset = HardDrive.changeset(%HardDrive{}, hard_drive_params)
  #
  #   case Repo.insert(changeset) do
  #     {:ok, hard_drive} ->
  #       conn
  #       |> put_status(:created)
  #       |> put_resp_header("location", hard_drive_path(conn, :show, hard_drive))
  #       |> render("show.json", hard_drive: hard_drive)
  #     {:error, changeset} ->
  #       conn
  #       |> put_status(:unprocessable_entity)
  #       |> render(Nimbus2000.ChangesetView, "error.json", changeset: changeset)
  #   end
  # end
  #
  # def show(conn, %{"id" => id}) do
  #   hard_drive = Repo.get!(HardDrive, id)
  #   render(conn, "show.json", hard_drive: hard_drive)
  # end
  #
  # def update(conn, %{"id" => id, "hard_drive" => hard_drive_params}) do
  #   hard_drive = Repo.get!(HardDrive, id)
  #   changeset = HardDrive.changeset(hard_drive, hard_drive_params)
  #
  #   case Repo.update(changeset) do
  #     {:ok, hard_drive} ->
  #       render(conn, "show.json", hard_drive: hard_drive)
  #     {:error, changeset} ->
  #       conn
  #       |> put_status(:unprocessable_entity)
  #       |> render(Nimbus2000.ChangesetView, "error.json", changeset: changeset)
  #   end
  # end
  #
  # def delete(conn, %{"id" => id}) do
  #   hard_drive = Repo.get!(HardDrive, id)
  #
  #   # Here we use delete! (with a bang) because we expect
  #   # it to always work (and if it does not, it will raise).
  #   Repo.delete!(hard_drive)
  #
  #   send_resp(conn, :no_content, "")
  # end
end
