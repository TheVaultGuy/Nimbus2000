defmodule Nimbus2000.PageController do
  use Nimbus2000.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end

  def error404(conn, _params) do
    conn
    |> put_flash(:error, "ERROR 404: Good Job. You BROKE IT!")
    |> render("404.html")
  end
end
