defmodule Nimbus2000.PasswordController do
  use Nimbus2000.Web, :controller

  alias Nimbus2000.User

  def new(conn, _) do
    conn
    |> put_flash(:error, "Unfortunately, we don't support resetting passwords yet. Please email michaely@film-45.com")
    |> render(:new)
  end

  def reset_password(conn, %{"user" => %{"email" => email}}) do
    conn
    |> put_flash(:info, "Password reset link has been sent to your email address.")
    |> redirect(to: session_path(conn, :new))
  end

  def reset(conn, %{"user" => %{"email" => email}}) do
    if String.trim(email) == "" do
      conn
      |> put_flash(:error, "Please enter your email")
      |> redirect(to: password_path(conn, :new))
    else
      conn
      |> put_flash(:error, "Unfortunately, we don't support resetting passwords yet. Please email michaely@film-45.com")
      |> redirect(to: session_path(conn, :new))
    end
  end
end
