defmodule Nimbus2000.RegistrationController do
  use Nimbus2000.Web, :controller

  alias Nimbus2000.User


  def new(conn, _params) do
    changeset = User.changeset(%Nimbus2000.User{})
    conn
    |> check_login()
    |> render(:new, changeset: changeset)
  end

  def create(conn, %{"user" => registration_params}) do
    oldEmail = registration_params["email"]
    changeset = User.changeset(%User{}, %{registration_params | "email" => String.downcase(oldEmail)})
    case Repo.insert(changeset) do
      {:ok, user} ->
        conn
        |> put_flash(:success, "Account created! Please log in as " <> user.email)
        |> put_flash(:value, user.email)
        |> redirect(to: session_path(conn, :new))
      {:error, changeset} ->
        conn
        |> render(:new, changeset: changeset)
    end
  end

  defp check_login(conn) do
    if conn.assigns[:current_user] do
      conn
      |> redirect(to: page_path(conn, :index))
      |> halt
    else
      conn
    end
  end

end
