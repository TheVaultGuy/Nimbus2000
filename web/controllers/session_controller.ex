defmodule Nimbus2000.SessionController do
  use Nimbus2000.Web, :controller

  alias Nimbus2000.User
  alias Passport.Session


  def new(conn, _) do
    conn
    |> check_login()
    |> render(:new)
  end

  def create(conn, %{"session" => %{"email" => email, "password" => pass}}) do
    check_login(conn)
    case Session.login(conn, String.downcase(email), pass) do
    {:ok, conn} ->
      conn
      |> redirect(to: page_path(conn, :index))
    {:error, _reason, conn} ->
      conn
      |> put_flash(:error, "Invalid username/password combination")
      |> render(:new)
    end
  end

  def delete(conn, _) do
    conn
    |> Session.logout()
    |> put_flash(:info, "You have been logged out")
    |> redirect(to: session_path(conn, :new))
  end

  defp check_login(conn) do
    if conn.assigns[:current_user] do
      conn
      |> redirect(to: page_path(conn, :index))
      |> halt
    else
      conn
    end
  end
end
