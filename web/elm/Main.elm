module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Utility.Keys as Keys
import Utility.KeyboardEvents as KeyboardEvents
import Model.Crud as Crud exposing (..)
import RouteUrl exposing (RouteUrlProgram)
import Phoenix.Socket
import Phoenix.Channel
import Phoenix.Push
import Task
import Json.Encode as Encode
import Json.Decode as Decode
import Model.User as User exposing (User)
import Model.Project as Project exposing (Project)
import Model.Item
import Debug
import ModelMsg exposing (..)
import String
import Rainfall.Searcher
import Rainfall.Viewer
import Model.Item as Item
import Set
import Router
import RouteUrl.Builder exposing (builder, modifyEntry, newEntry, replacePath, toUrlChange)
import Navigation
import Rainfall.Viewer.Util as Util


--Main


main : RouteUrlProgram Flags Model Msg
main =
    RouteUrl.programWithFlags
        { delta2url = Router.delta2url
        , location2messages = Router.location2messages
        , init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }



-- Model


type alias Flags =
    { token : String
    , logout : String
    , home : String
    , server : String
    }



-- Init


init : Flags -> ( Model, Cmd Msg )
init flags =
    ( { debug = ""
      , token = flags.token
      , user = Nothing
      , profile_pic_url = ""
      , logout = flags.logout
      , home = flags.home
      , socket = initSocket flags.server
      , search = Rainfall.Searcher.emptySearch
      , toasts = []
      , keyCombo = KeyboardEvents.emptyCombo
      , item = Nothing
      , users = []
      , people = []
      , projects = []
      , drives = []
      , scans = []
      , media = []
      , transcriptions = []
      , ditReports = []
      , assets = []
      , purchases = []
      , ccReports = []
      }
    , Cmd.batch
        [ Task.perform (always JoinChannel) (Task.succeed ()) ]
    )


initSocket : String -> Phoenix.Socket.Socket Msg
initSocket server =
    Phoenix.Socket.init server
        |> Phoenix.Socket.withDebug
        |> Phoenix.Socket.on "shout" "room:lobby" ShoutIn
        |> Phoenix.Socket.on "crud" "room:lobby" CRUDIn



-- Update


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoMsg ->
            ( model, Debug.log "NoMsg Called" Cmd.none )

        ShoutIn raw ->
            let
                incomingDecoder : Decode.Decoder String
                incomingDecoder =
                    Decode.map (identity)
                        (Decode.field "body" Decode.string)
            in
                case Decode.decodeValue incomingDecoder raw of
                    Ok shoutMessage ->
                        { model | debug = shoutMessage } ! []

                    Err error ->
                        { model | debug = error } ! []

        ShoutOut str ->
            let
                payload =
                    Encode.object [ ( "body", Encode.string str ) ]

                pushTo =
                    Phoenix.Push.init "shout" "room:lobby"
                        |> Phoenix.Push.withPayload payload

                ( newSocket, phoenixCmd ) =
                    Phoenix.Socket.push pushTo model.socket
            in
                { model | socket = newSocket } ! [ Cmd.map PhoenixMsg phoenixCmd ]

        CRUDIn raw ->
            let
                oldSearch =
                    model.search

                newSearch =
                    oldSearch
            in
                { model | search = newSearch } ! [ Cmd.none ]

        CRUDOut crud ->
            let
                crudChannel =
                    Crud.crudToStr crud

                payload =
                    Crud.messageEncoder crud

                pushTo =
                    Phoenix.Push.init crudChannel "room:lobby"
                        |> Phoenix.Push.withPayload payload
                        |> Phoenix.Push.onError (\errormsg -> Debug.log (Encode.encode 4 errormsg) NoMsg)
                        |> Phoenix.Push.onOk (\okMsg -> ReadIn okMsg)

                ( newSocket, phoenixCmd ) =
                    Phoenix.Socket.push pushTo model.socket
            in
                { model | socket = newSocket } ! [ Cmd.map PhoenixMsg phoenixCmd ]

        ReadIn payload ->
            let
                oldSearch =
                    model.search

                newResults =
                    case Decode.decodeValue (Decode.field "body" Model.Item.itemDecoder) payload of
                        Ok results ->
                            results

                        Err error ->
                            Debug.log ("Read Reply Error: " ++ (toString error)) []

                newSearch =
                    { oldSearch | results = newResults, busy = False }
            in
                { model | search = newSearch } ! [ Cmd.none ]

        JoinChannel ->
            let
                token =
                    Encode.object
                        [ ( "token", Encode.string model.token )
                        ]

                newChannel =
                    Phoenix.Channel.init "room:lobby"
                        |> Phoenix.Channel.withPayload token
                        |> Phoenix.Channel.onJoin JoinSuccess
                        |> Phoenix.Channel.onJoinError JoinError
                        |> Phoenix.Channel.onError ConnectionError
                        |> Phoenix.Channel.onClose ConnectionClose

                ( newSocket, phoenixCmd ) =
                    Phoenix.Socket.join newChannel model.socket
            in
                { model | socket = newSocket } ! [ Cmd.map PhoenixMsg phoenixCmd ]

        JoinSuccess raw ->
            case (User.decodeUser raw) of
                Ok user ->
                    update (OnSearch model.search) { model | user = Just (Debug.log "user:" user) }

                Err error ->
                    { model | debug = error } ! [{- Navigation.load model.logout- -}]

        JoinError raw ->
            { model | debug = "Unable to Connect" } ! [ Navigation.load model.logout ]

        ConnectionError raw ->
            model ! []

        ConnectionClose raw ->
            model ! []

        PhoenixMsg msg ->
            let
                ( newSocket, phoenixCmd ) =
                    Phoenix.Socket.update msg model.socket
            in
                { model | socket = newSocket }
                    ! [ Cmd.map PhoenixMsg phoenixCmd ]

        UpdateKeyCombo newCombo ->
            { model | keyCombo = newCombo.combo }
                ! [ KeyboardEvents.mapList newCombo
                        [ ( KeyboardEvents.Down, [ Keys.Space, Keys.Shift Nothing ], CRUDOut, Delete <| DeleteMsg Users )
                        , ( KeyboardEvents.Down, [ Keys.T ], CRUDOut, Read <| ReadMsg Crud.Users Crud.ListView [] )
                        , ( KeyboardEvents.Down, [ Keys.S ], CRUDOut, Delete <| DeleteMsg Users )
                        ]
                  ]

        OnChangeTable table ->
            let
                oldSearch =
                    model.search

                newSearch =
                    { oldSearch | table = table, results = [], selected = [], sortAlgorithm = \a b -> EQ, sortColumn = "" }
            in
                update (OnSearch newSearch) { model | search = newSearch }

        OnSearch search ->
            let
                newSearch =
                    { search | busy = True, results = [] }

                preload =
                    Crud.ListView

                filters =
                    []

                readMsg =
                    { table = search.table, preload = preload, filters = filters }
            in
                update (CRUDOut <| Read readMsg) { model | search = newSearch }

        OnSelect item ->
            let
                oldSearch =
                    model.search

                newSearch =
                    if KeyboardEvents.equals (Set.fromList [ Maybe.withDefault -1 <| Keys.code <| Keys.Ctrl Nothing ]) model.keyCombo then
                        { oldSearch | selected = item :: oldSearch.selected }
                    else if KeyboardEvents.equals (Set.fromList [ Maybe.withDefault -1 <| Keys.code <| Keys.Shift Nothing ]) model.keyCombo then
                        -- Fuck this is going to be so complicated
                        let
                            shiftSelector next ( added, new ) =
                                if ( added, new ) == ( [], [] ) then
                                    if (next == item || List.member next oldSearch.selected) then
                                        ( [ next ], [] )
                                    else
                                        ( [], [] )
                                else if (next == item || List.member next oldSearch.selected) then
                                    ( item :: (added ++ new), [] )
                                else
                                    ( added, item :: new )

                            ( newSelected, trash ) =
                                oldSearch.results
                                    |> List.sortWith oldSearch.sortAlgorithm
                                    |> List.foldl shiftSelector ( [], [] )
                        in
                            { oldSearch | selected = newSelected }
                    else
                        { oldSearch | selected = [ item ] }
            in
                { model | search = newSearch } ! []

        OnDeselect item ->
            let
                oldSearch =
                    model.search

                newSearch =
                    if KeyboardEvents.equals (Set.fromList [ Maybe.withDefault -1 <| Keys.code <| Keys.Ctrl Nothing ]) model.keyCombo then
                        { oldSearch | selected = List.filter (\x -> x /= item) oldSearch.selected }
                    else
                        { oldSearch | selected = [ item ] }
            in
                { model | search = newSearch } ! []

        SetUrl change ->
            case change.entry of
                RouteUrl.NewEntry ->
                    model ! [ Navigation.newUrl change.url ]

                RouteUrl.ModifyEntry ->
                    model ! [ Navigation.modifyUrl <| change.url ]

        SearchMsg msg ->
            let
                oldSearch =
                    model.search
            in
                { model | search = (Rainfall.Searcher.update msg oldSearch) } ! []

        On404 ->
            { model | user = Nothing } ! [ Navigation.load "/404" ]



-- _ ->
--     { model | toasts = [ Warning ( 1, "ERROR 404 - Content not found!" ) ] } ! []
-- View


view : Model -> Html.Html Msg
view model =
    case model.user of
        Nothing ->
            if model.debug /= "" then
                div [ class "centered empty shadow text-center col-6 col-md-8 mt-40" ]
                    [ div [ class "empty-title" ] [ h4 [ class "vertical-align-text" ] [ Util.icon "warning", text "Error!" ] ]
                    ]
            else
                div [ class "centered empty shadow text-center col-6 col-md-8 mt-40" ]
                    [ div [ class "loading" ] []
                    , div [ class "empty-title" ] [ h4 [] [ text "Loading" ] ]
                    ]

        Just user ->
            viewContainer model <| Rainfall.Viewer.listView model.search user.admin


viewContainer : Model -> Html.Html Msg -> Html.Html Msg
viewContainer model insides =
    let
        fullname =
            case model.user of
                Just user ->
                    user.first_name ++ " " ++ user.last_name

                Nothing ->
                    ""

        initials =
            case model.user of
                Just user ->
                    String.toUpper ((String.left 1 user.first_name) ++ (String.left 1 user.last_name))

                Nothing ->
                    ""

        avatarURL =
            if model.profile_pic_url /= "" then
                --"http://www.avatarpro.biz/avatar/" ++ fullname ++ "?s=256"
                "http://collegebowl.avatarpro.biz/avatar.png"
            else
                model.profile_pic_url

        avatar =
            [ img [ class "avatar-img-alt", src (avatarURL), attribute "onerror" "this.style.display='none'" ] []
            ]
    in
        div [ class "fill-height fade-in-slow" ]
            [ div [ class "flex-wrapper col-12 shadow bg-white-50" ]
                [ div [ class "centered col-12 pb-10 stopScrollPropagation" ]
                    [ div [ class "col-10 col-xl-12 container bg-white flex col-oneline rounded light-shadow" ]
                        [ div [ class "centered" ]
                            [ a [ class "revert centered", href model.home ]
                                [ h1 [ class "mt-5 mb-5" ]
                                    [ text "The Magi "
                                    , span [ class "hide-lg" ]
                                        [ h5 [ class "inline" ]
                                            [ b [ class "logo" ]
                                                [ text "FILM"
                                                , span [ class "orange" ] [ text " 45" ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        , h6 [ class "mt-5 mb-5" ]
                            [ span [ class "hide-sm" ] [ text fullname ]
                            , a [ href model.logout ]
                                [ figure [ class "avatar avatar-xl ml-10 bg-info", attribute "data-initial" initials, attribute "hover-text" "Log Out" ]
                                    avatar
                                ]
                            ]
                        , div [ class "clearfix" ] []
                        ]
                    ]
                , insides
                ]
            , div [ style [ ( "height", "10vh" ) ] ] []
            , viewCredit
            ]


viewDebug : String -> Html.Html Msg
viewDebug str =
    div [ class "toast green bg-black mt-10 vertical-align-text" ] [ Util.iconSmall "code", text str ]


viewCredit : Html.Html Msg
viewCredit =
    div [ class "credit-block" ]
        [ img [ class "img-responsive centered", src "images/peteberg.png" ] []
        , div [ class "silver bg-black" ] [ div [ class "vertical-align-text" ] [ Util.iconSmall "favorite", text "Elm & Phoenix" ] ]
        , div [ class "silver bg-black" ]
            [ div [ class "vertical-align-text" ]
                [ Util.iconSmall "computer"
                , text "Made by Michael Yuan"
                , span [ class "blinker" ] [ text "|" ]
                ]
            ]
        ]



-- Subscriptions


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Phoenix.Socket.listen model.socket PhoenixMsg
        , KeyboardEvents.subscribe model.keyCombo UpdateKeyCombo
        ]
