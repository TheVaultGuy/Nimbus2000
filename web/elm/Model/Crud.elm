module Model.Crud exposing (..)

import Json.Encode as Encode
import Dict exposing (Dict)


type Crud
    = Create CreateMsg
    | Read ReadMsg
    | Update UpdateMsg
    | Delete DeleteMsg


type Table
    = Users
    | Projects
    | Drives
    | Media
    | Assets
    | Purchases
    | Scans
    | Transcriptions
    | People
    | CC_Reports
    | DIT_Reports


type Preload
    = IconView
    | ListView
    | EditView


type alias GenericLink =
    { table : Table
    , id : String
    }


type Link
    = Generic GenericLink
    | NoLink


type JoinTable
    = PeopleProjects -- AKA Permissions
    | AssetScans
    | AssetPurchases
    | DriveMedia --AKA Folderpath
    | DIT_ReportsMedia
    | MediaTranscriptions


generalTables : List Table
generalTables =
    [ Projects
    , Assets
    , People
    ]


aeTables : List Table
aeTables =
    [ Projects
    , Drives
    , Media
    , Transcriptions
    , DIT_Reports
    ]


accountingTables : List Table
accountingTables =
    [ Projects
    , Assets
    , Purchases
    , People
    , CC_Reports
    ]


adminTables : List Table
adminTables =
    [ Users
    , People
    , Projects
    , Drives
    , Scans
    , Media
    , Transcriptions
    , DIT_Reports
    , Assets
    , Purchases
    , CC_Reports
    ]


type alias CreateMsg =
    { table : Table
    , data : Dict String Encode.Value
    , links : List Link
    }


type alias ReadMsg =
    { table : Table
    , preload : Preload
    , filters : List String
    }


type alias UpdateMsg =
    { table : Table
    , data : Dict String Encode.Value
    }


type alias DeleteMsg =
    { table : Table
    }


messageEncoder : Crud -> Encode.Value
messageEncoder crud =
    let
        encodeList =
            case crud of
                Create msg ->
                    [ ( "table", Encode.string (tableToStr msg.table) )
                    ]

                Read msg ->
                    [ ( "table", Encode.string <| tableToStr msg.table )
                    , ( "preload", Encode.string <| preloadToStr msg.preload )
                    , ( "filters", Encode.list <| List.map Encode.string <| msg.filters )
                    ]

                Update msg ->
                    [ ( "table", Encode.string (tableToStr msg.table) )
                    ]

                Delete msg ->
                    [ ( "table", Encode.string (tableToStr msg.table) )
                    ]
    in
        Encode.object encodeList


tableToStr : Table -> String
tableToStr table =
    String.toLower <| toString table


preloadToStr : Preload -> String
preloadToStr preload =
    String.toLower <| toString preload


crudToStr : Crud -> String
crudToStr crud =
    case crud of
        Create a ->
            "create"

        Read a ->
            "read"

        Update a ->
            "update"

        Delete a ->
            "delete"
