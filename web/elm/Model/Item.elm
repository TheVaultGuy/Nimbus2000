module Model.Item exposing (..)

import Model.Project exposing (Project)
import Model.User exposing (User)
import Model.Person exposing (Person)
import Json.Decode as Decode


type Item
    = ProjectItem Project
    | DriveItem
    | MediumItem
    | DocumentItem
    | UserItem User
    | PersonItem Person
    | ErrorItem


itemDecoder : Decode.Decoder (List Item)
itemDecoder =
    let
        decoders =
            List.map (\x -> Decode.list x)
                [ Decode.map ProjectItem Model.Project.projectDecoder
                , Decode.map UserItem Model.User.userDecoder
                , Decode.map PersonItem Model.Person.personDecoder
                , Decode.fail "improperly formatted data"
                ]
    in
        Decode.oneOf decoders
