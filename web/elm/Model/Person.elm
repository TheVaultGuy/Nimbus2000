module Model.Person exposing (..)

import Json.Encode as Encode
import Json.Decode as Decode


type alias Person =
    { person_id : Int
    , user_id : Int
    , first_name : String
    , last_name : String
    , company : String
    , bio : String
    , notes : String
    , profile_pic_url : String
    , projects : List String
    }


personDecoder : Decode.Decoder Person
personDecoder =
    Decode.succeed Person
        |> Decode.map2 (|>) (Decode.field "person_id" Decode.int)
        |> Decode.map2 (|>) (Decode.field "user_id" Decode.int)
        |> Decode.map2 (|>) (Decode.field "first_name" Decode.string)
        |> Decode.map2 (|>) (Decode.field "last_name" Decode.string)
        |> Decode.map2 (|>) (Decode.field "company" Decode.string)
        |> Decode.map2 (|>) (Decode.field "bio" Decode.string)
        |> Decode.map2 (|>) (Decode.field "notes" Decode.string)
        |> Decode.map2 (|>) (Decode.field "profile_pic_url" Decode.string)
        |> Decode.map2 (|>) (Decode.field "projects" <| Decode.list Decode.string)


decodePerson : Decode.Value -> Result String Person
decodePerson raw =
    Decode.decodeValue personDecoder raw


encodePerson : Person -> Encode.Value
encodePerson person =
    Encode.object
        [ ( "person_id", Encode.int person.person_id )
        , ( "user_id", Encode.int person.user_id )
        , ( "first_name", Encode.string person.first_name )
        , ( "last_name", Encode.string person.last_name )
        , ( "company", Encode.string person.company )
        , ( "bio", Encode.string person.bio )
        , ( "notes", Encode.string person.notes )
        , ( "profile_pic_url", Encode.string person.profile_pic_url )
        ]
