module Model.Project exposing (..)

import Json.Encode as Encode
import Json.Decode as Decode


type alias Project =
    { number : Int
    , code : String
    , title : String
    , company : String
    , about : String
    , status : Status
    }


type Status
    = Development
    | Preproduction
    | Production
    | Postproduction
    | Inactive


icon : String
icon =
    " fa fa-folder-open"


projectDecoder : Decode.Decoder Project
projectDecoder =
    Decode.map6 Project
        (Decode.field "number" Decode.int)
        (Decode.field "code" Decode.string)
        (Decode.field "title" Decode.string)
        (Decode.field "company" Decode.string)
        (Decode.field "about" Decode.string)
        (Decode.field "status"
            (Decode.map
                (\str ->
                    if str == "in development" then
                        Development
                    else if str == "in pre-production" then
                        Preproduction
                    else if str == "in production" then
                        Production
                    else if str == "in post-production" then
                        Postproduction
                    else
                        Inactive
                )
                Decode.string
            )
        )


projectListDecoder : Decode.Decoder (List Project)
projectListDecoder =
    Decode.field "projects" (Decode.list projectDecoder)


decodeProject : Decode.Value -> Result String Project
decodeProject raw =
    Decode.decodeValue projectDecoder raw


decodeProjectList : Decode.Value -> Result String (List Project)
decodeProjectList raw =
    Decode.decodeValue projectListDecoder raw


encodeProject : Project -> Encode.Value
encodeProject proj =
    Encode.object
        [ ( "number", Encode.int proj.number )
        , ( "code", Encode.string proj.code )
        , ( "title", Encode.string proj.title )
        , ( "company", Encode.string proj.company )
        , ( "about", Encode.string proj.about )
        , ( "status"
          , Encode.string
                ((\statusRaw ->
                    case statusRaw of
                        Development ->
                            "in development"

                        Preproduction ->
                            "in pre-production"

                        Production ->
                            "in production"

                        Postproduction ->
                            "in post-production"

                        Inactive ->
                            "inactive"
                 )
                    proj.status
                )
          )
        ]
