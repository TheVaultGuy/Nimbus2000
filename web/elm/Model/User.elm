module Model.User exposing (..)

import Json.Encode as Encode
import Json.Decode as Decode


type alias User =
    { first_name : String
    , last_name : String
    , email : String
    , bio : String
    , person_id : Int
    , projects : List String
    , admin : Bool
    , active : Bool
    }


userDecoder : Decode.Decoder User
userDecoder =
    Decode.map8 User
        (Decode.field "first_name" Decode.string)
        (Decode.field "last_name" Decode.string)
        (Decode.field "email" Decode.string)
        (Decode.field "bio" Decode.string)
        (Decode.field "person_id" Decode.int)
        (Decode.field "projects" <| Decode.list Decode.string)
        (Decode.field "admin" Decode.bool)
        (Decode.field "active" Decode.bool)


decodeUser : Decode.Value -> Result String User
decodeUser raw =
    Decode.decodeValue userDecoder raw


encodeUser : User -> Encode.Value
encodeUser user =
    Encode.object
        [ ( "email", Encode.string user.email )
        , ( "person_id", Encode.int user.person_id )
        , ( "projects", Encode.list <| List.map Encode.string user.projects )
        , ( "admin", Encode.bool user.admin )
        , ( "active", Encode.bool user.active )
        ]
