module ModelMsg exposing (..)

import Phoenix.Socket
import Json.Encode as Encode
import Json.Decode as Decode
import Utility.KeyboardEvents as KeyboardEvents
import Model.Crud as Crud
import Rainfall.Searcher
import Model.Item as Item
import RouteUrl exposing (UrlChange, HistoryEntry(..))
import Model.User as User exposing (User)
import Model.Person as Person exposing (Person)
import Model.Project as Project exposing (Project)
import Model.Asset as Asset exposing (Asset)
import Model.CCreport as CCreport exposing (CCreport)
import Model.DITreport as DITreport exposing (DITreport)
import Model.Drive as Drive exposing (Drive)
import Model.Media as Media exposing (Media)
import Model.Purchase as Purchase exposing (Purchase)
import Model.Scan as Scan exposing (Scan)
import Model.Transcription as Transcription exposing (Transcription)


-- import Html exposing (..)


type Msg
    = NoMsg
    | ShoutIn Encode.Value
    | ShoutOut String
    | CRUDIn Encode.Value
    | CRUDOut Crud.Crud
    | ReadIn Decode.Value
    | JoinChannel
    | JoinSuccess Decode.Value
    | JoinError Decode.Value
    | ConnectionError Decode.Value
    | ConnectionClose Decode.Value
    | PhoenixMsg (Phoenix.Socket.Msg Msg)
    | UpdateKeyCombo KeyboardEvents.KeyComboEvent
    | OnChangeTable Crud.Table
    | OnSearch Rainfall.Searcher.Search
    | OnSelect Item.Item
    | OnDeselect Item.Item
    | SearchMsg Rainfall.Searcher.SearchMsg
    | On404
    | SetUrl UrlChange


type Toast
    = Success ( Int, String )
    | Info ( Int, String )
    | Warning ( Int, String )


type alias Model =
    { debug : String
    , token : String
    , user : Maybe User
    , profile_pic_url : String
    , logout : String
    , home : String
    , socket : Phoenix.Socket.Socket Msg
    , search :
        Rainfall.Searcher.Search

    -- Data Management
    , item : Maybe Item.Item
    , toasts : List Toast
    , keyCombo : KeyboardEvents.KeyCombo
    , users : List User
    , people : List Person
    , projects : List Project
    , drives : List Drive
    , scans : List Scan
    , media : List Media
    , transcriptions : List Transcription
    , ditReports : List DITreport
    , assets : List Asset
    , purchases : List Purchase
    , ccReports : List CCreport
    }
