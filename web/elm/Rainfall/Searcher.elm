module Rainfall.Searcher exposing (..)

import Model.Item as Item exposing (Item(..))
import Model.Crud as Crud
import Model.Project as Project exposing (Status(..))


type ModalType
    = NoModal
    | Actions
    | Filters
    | Staging
    | Edit (List Item)
    | Create Item --pass in the default item
    | View (List Item)
    | Confirm ( String, String, ( String, String ) ) --(title, message, (yes, no))
    | AdvancedSearch


type SearchMsg
    = SetModal ModalType
    | SetSearchString String
    | SetFilters (List Filter)
    | NoMsg


type alias Search =
    { table : Crud.Table
    , filters : List Filter
    , results : List Item.Item
    , saved : List Item.Item
    , selected : List Item.Item
    , sortAlgorithm : Item.Item -> Item.Item -> Order
    , sortColumn : String
    , sortReverse : Bool
    , hideUnstaged : Bool
    , activeModal : ModalType
    , searchString : String
    , busy : Bool
    }


update : SearchMsg -> Search -> Search
update msg search =
    case msg of
        SetModal modal ->
            { search | activeModal = modal }

        SetSearchString str ->
            { search | searchString = str }

        SetFilters filters ->
            { search | filters = filters }

        _ ->
            search


emptySearch : Search
emptySearch =
    { table = Crud.Projects
    , filters = []
    , results = []
    , saved = []
    , selected = []
    , sortAlgorithm = \a b -> EQ
    , sortColumn = ""
    , sortReverse = False
    , hideUnstaged = False
    , activeModal = NoModal
    , searchString = ""
    , busy = False
    }


type Filter
    = Column ( Int, String )
    | Range ( Int, ( String, String ) )
    | Keyword String
