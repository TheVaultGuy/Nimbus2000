module Rainfall.Viewer exposing (..)

import Model.Item as Item exposing (Item(..))
import ModelMsg exposing (Msg(..))
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Rainfall.Searcher as Searcher exposing (ModalType(..))
import Model.Crud as Crud exposing (Table(..))
import Json.Decode as Decode
import Model.Project exposing (Project)
import Rainfall.Viewer.Project
import Model.User exposing (User)
import Rainfall.Viewer.User
import Model.Person exposing (Person)
import Rainfall.Viewer.Person
import Regex exposing (HowMany)
import Rainfall.Viewer.Util as Util


--import Model.Drive exposing (Drive)
-- import Model.Media exposing (Media)


type FieldType
    = Text
    | EditableText
    | Boolean Bool
    | EditableBoolean
    | Link ModelMsg.Msg
    | EditableLink ModelMsg.Msg


tabView : Searcher.Search -> Html.Html ModelMsg.Msg
tabView search =
    let
        tabList =
            Crud.adminTables

        --[ Users, Projects, Drives, Media ]
        toTabName table =
            Regex.replace Regex.All (Regex.regex "_") (\_ -> " ") (toString table)

        toTabs table =
            if table == search.table then
                li [ class "tab-item active bg-white" ] [ a [ class "active btn-link" ] [ text (toTabName table) ] ]
            else
                li [ class "tab-item bg-white" ] [ a [ onClick (OnChangeTable table) ] [ text (toTabName table) ] ]
    in
        ul [ class "tab tab-block sticky mb-10 rounded light-shadow", style [ ( "margin-top", "0" ), ( "overflow-x", "auto" ), ( "overflow-y", "hidden" ) ] ] (List.map toTabs tabList)


listView : Searcher.Search -> Bool -> Html.Html ModelMsg.Msg
listView search admin =
    let
        sortedList =
            List.sortWith search.sortAlgorithm search.results

        toFieldName name =
            if name == search.sortColumn then
                if search.sortReverse then
                    th [] [ button [ class "btn-link", style [ ( "color", "white" ) ] ] [ text name ], Util.icon "keyboard_arrow_up" ]
                else
                    th [] [ button [ class "btn-link", style [ ( "color", "white" ) ] ] [ text name ], Util.icon "keyboard_arrow_down" ]
            else
                th [] [ button [ class "btn-link", style [ ( "color", "white" ) ] ] [ text name ] ]

        listHead =
            case search.table of
                Projects ->
                    List.map toFieldName Rainfall.Viewer.Project.columns

                Users ->
                    List.map toFieldName Rainfall.Viewer.User.columns

                People ->
                    List.map toFieldName Rainfall.Viewer.Person.columns

                _ ->
                    [ text "TO DO" ]

        toRow item =
            let
                toFields item =
                    case item of
                        ProjectItem project ->
                            Rainfall.Viewer.Project.listView project

                        UserItem user ->
                            Rainfall.Viewer.User.listView user

                        PersonItem person ->
                            Rainfall.Viewer.Person.listView person

                        _ ->
                            [ text "TO DO" ]
            in
                if List.member item search.selected then
                    tr [ class "selected outline fade-in-zoom", onClick (OnDeselect item) ] (toFields item)
                else
                    tr [ class "fade-in-zoom", onClick (OnSelect item) ] (toFields item)

        mainTable =
            if search.busy then
                [ table [ class "table table-striped table-hover" ]
                    [ thead [ class "white sticky light-shadow" ] [ tr [ class "bg-secondary" ] listHead ] ]
                , div [ class "empty silver fill-height col-12", style [ ( "background", "transparent" ) ] ]
                    [ div [ class "loading" ] []
                    , div [ class "empty-title" ] [ h4 [] [ text "Reading Database" ] ]
                    ]
                ]
            else
                [ table [ class "table table-striped table-hover" ]
                    [ thead [ class "white sticky light-shadow" ] [ tr [ class "bg-secondary" ] listHead ]
                    , tbody [ class "table-animated" ] <| List.map toRow sortedList
                    ]
                ]
    in
        div [ class "flex-fill columns col-oneline mb-10 hide-scrollers stopScrollPropagation", style [ ( "width", "100%" ), ( "margin", "0" ) ] ]
            [ div [ class "col-3 col-lg-1 col-sm-2 col-xs-3" ] [ toolView search admin ]
            , div [ class "col-8 col-xl-9 col-lg-11 col-sm-10 col-xs-9 fill-height flex-wrapper" ]
                [ tabView search
                , div [ class "flex-fill light-shadow bg-meta stopScrollPropagation pb-20 rounded", style [ ( "height", "100%" ), ( "overflow", "auto" ), ( "border-style", "solid" ), ( "border-width", "0 0 2rem 0" ), ( "z-index", "100;" ) ] ]
                    mainTable
                , div [ class "stopScrollPropagation pt-10" ] []
                ]
            ]


itemView : Item.Item -> Bool -> Html.Html ModelMsg.Msg
itemView item editable =
    let
        modalInsides =
            if editable then
                div [] [ text "Congrats! You can edit this." ]
            else
                div [] [ text "Too Bad! You don't have the necessary permissions." ]
    in
        div [ class "modal active" ]
            [ div [ class "modal-overlay" ] []
            , div [ class "modal-container" ]
                [ div [ class "modal-header" ]
                    [ button [ class "btn btn-clear float-right" ] []
                    , div [ class "modal-title" ] [ text "Item Details" ]
                    ]
                , div [ class "modal-body" ] [ div [ class "content" ] [ modalInsides ] ]
                , div [ class "modal-footer" ]
                    [ button [ class "btn btn-link" ] [ text "Close" ]
                    , button [ class "btn btn-primary" ] [ text "Save" ]
                    ]
                ]
            ]


toolView : Searcher.Search -> Bool -> Html.Html ModelMsg.Msg
toolView search admin =
    let
        savedView item =
            case item of
                ProjectItem project ->
                    div [ class "chip" ] [ text "test" ]

                _ ->
                    text ""

        filterChipView filter =
            case filter of
                Searcher.Column ( index, searchTerm ) ->
                    if index < 0 then
                        div [ class "chip-sm bg-silver text-ellipsis" ] [ text searchTerm, button [ class "btn btn-clear inline" ] [] ]
                    else
                        div [ class "chip-sm selected text-ellipsis" ] [ text <| "|" ++ searchTerm, button [ class "btn btn-clear inline" ] [] ]

                Searcher.Range ( index, ( x, y ) ) ->
                    let
                        small =
                            Basics.min x y

                        big =
                            Basics.max x y
                    in
                        div [ class "chip-sm bg-silver selected" ] [ text ((toString x) ++ " to " ++ (toString y)), button [ class "btn btn-clear inline" ] [] ]

                Searcher.Keyword searchTerm ->
                    div [ class "chip-sm bg-silver text-ellipsis" ] [ text searchTerm, button [ class "btn btn-clear inline" ] [] ]

        normalSearchView =
            input [ class "form-input", type_ "text", placeholder "Search...", autofocus True ] []

        actionControls =
            let
                adminTools =
                    if admin then
                        [ li [ class "menu-header" ]
                            [ span [ class "menu-header-text" ] [ text "Admin Tools" ]
                            ]
                        , button [ class "btn btn-primary btn-lg" ] [ text "Scan Drives" ]
                        ]
                    else
                        []
            in
                div [ class "card mb-10 light-shadow" ]
                    [ div [ class "card-header" ]
                        [ div [ class "card-title" ] [ text "Actions" ]
                        , div [ class "card-meta" ]
                            [ text ((toString <| List.length search.selected) ++ " items selected") ]
                        ]
                    , ul [ class "menu no-shadow" ]
                        [ div [] adminTools
                        , li [ class "menu-header" ]
                            [ span [ class "menu-header-text" ] [ text "General" ]
                            ]
                        , li [ class "menu-item" ]
                            [ a [] [ text "Add" ]
                            , a [] [ text "Edit" ]
                            , a [] [ text "Stage" ]
                            , a [] [ text "Delete" ]
                            ]
                        ]
                    ]

        filterControls =
            div [ class "card mb-10 light-shadow" ]
                [ div [ class "card-header" ]
                    [ div [ class "card-title" ] [ text "Filters" ]
                    , div [ class "card-meta" ]
                        [ text ((toString <| List.length search.results) ++ " items found") ]
                    ]
                , div [ class "card-footer" ]
                    [ div [ class "form-autocomplete" ]
                        [ div [ class "form-autocomplete-input" ]
                            [ div [ class "chip-sm selected text-ellipsis" ] [ text "butt stuff", button [ class "btn btn-clear inline" ] [] ]
                            , normalSearchView
                            ]
                        ]
                    ]
                ]

        stagingControls =
            div [ class "card mb-10 light-shadow" ]
                [ div [ class "card-header" ]
                    [ div [ class "card-title" ] [ text "Staging Area" ]
                    , div [ class "card-meta" ]
                        [ text ((toString <| List.length search.saved) ++ " items staged") ]
                    ]
                , div [ class "card-content pl-10 outline", style [ ( "min-height", "10rem" ) ] ]
                    [ div [ class "chip" ]
                        [ Util.icon "dns"
                        , div [ class "chip-content" ] [ text "content" ]
                        ]
                    , div [ class "chip" ]
                        [ Util.icon "dns"
                        , div [ class "chip-content" ] [ text "content" ]
                        ]
                    ]
                , div [ class "card-footer pb-5" ]
                    [ div [ class "form-group" ]
                        [ label [ class "form-checkbox" ]
                            [ input [ type_ "checkbox", checked search.hideUnstaged ] []
                            , i [ class "form-icon" ] []
                            , text "Hide Unstaged"
                            ]
                        ]
                    ]
                ]

        popoverBtn icon controls =
            let
                btn =
                    button
                        [ class "btn btn-lg col-12 mb-10"
                        , style [ ( "padding", ".1rem" ), ( "border-top-left-radius", "0" ), ( "border-bottom-left-radius", "0" ) ]
                        ]
                        [ Util.icon icon ]

                popover =
                    div [ class "popover-container" ] [ controls ]
            in
                div [ class "popover popover-top-right col-12" ]
                    [ btn
                    , popover
                    ]
    in
        div [ class "fill-height col-12" ]
            [ div [ class "hide-lg fill-height col-12 stopScrollPropagation pr-10 hide-scrollers offset-hide-l-xl offset-l-4", style [ ( "overflow", "auto" ) ] ]
                [ actionControls
                , filterControls
                , stagingControls
                ]
            , div [ class "show-lg fill-height col-12 stopScrollPropagation pt-5 pr-10" ]
                [ popoverBtn "touch_app" actionControls
                , popoverBtn "filter_list" filterControls
                , popoverBtn "inbox" stagingControls
                ]
            ]


type alias ToolsPanel =
    { filters : Filters
    , actions : Actions
    , staging : Staging
    }


type alias Filters =
    { fieldSelection : Int
    , rangeStart : String
    , rangeEnd : String
    , searchTerm : String
    }


type alias Actions =
    ()


type alias Staging =
    { hoveredItem : Item.Item
    }
