module Rainfall.Viewer.Person exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import ModelMsg exposing (..)
import Rainfall.Viewer.Util exposing (..)
import Model.Person exposing (Person)


columns : List String
columns =
    [ " ", "First Name", "Last Name", "Company", "Projects", "Bio", "Notes" ]


listView : Person -> List (Html Msg)
listView person =
    let
        profile_pic =
            if (person.profile_pic_url /= "") then
                i [ class "material-icons circle" ] [ text "account_circle" ]
            else
                img [ class "circle img-fit-cover", src person.profile_pic_url, attribute "onerror" "this.style.display='none'" ] []
    in
        [ td [] [ profile_pic ]
        , td [] [ text person.first_name ]
        , td [] [ text person.last_name ]
        , td [] [ text person.company ]
        , td [] [ listBox person.projects ]
        , td [] [ expandableText person.bio ]
        , td [] [ expandableText person.notes ]
        ]
