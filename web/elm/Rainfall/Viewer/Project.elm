module Rainfall.Viewer.Project exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import ModelMsg exposing (..)
import Rainfall.Viewer.Util exposing (..)
import Model.Project exposing (Project)

columns : List String
columns =
    [ "Code", "Number", "Title", "Company", "About", "Status" ]


listView : Project -> List (Html Msg)
listView project =
    [ td [] [ text project.code ]
    , td [] [ text <| toString project.number ]
    , td [] [ text project.title ]
    , td [] [ text project.company ]
    , td [] [ expandableText project.about ]
    , td [] [ text <| toString project.status ]
    ]

chipView : Project -> Html Msg
chipView project =
    div [ class "chip" ]
        [ i [ class "chip-icon fa fa-heart" ] []
        , div [ class "chip-content" ] [ text project.title ]
        ]
