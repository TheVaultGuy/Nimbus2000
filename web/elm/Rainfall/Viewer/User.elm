module Rainfall.Viewer.User exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import ModelMsg exposing (..)
import Rainfall.Viewer.Util exposing (..)
import Model.User exposing (User)


columns : List String
columns =
    [ "Name", "Email", "Details", "Projects", "Admin" ]


listView : User -> List (Html Msg)
listView user =
    [ td [] [ text (user.first_name ++ " " ++ user.last_name) ]
    , td [] [ text user.email ]
    , td [] [ hyperlink ("/Users/" ++ (toString user.person_id)) (user.first_name ++ " " ++ user.last_name) ]
    , td [] [ listBox user.projects ]
    , td [] [ check user.admin ]
    ]
