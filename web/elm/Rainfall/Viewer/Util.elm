module Rainfall.Viewer.Util exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import ModelMsg exposing (Msg(..))
import Json.Decode as Decode


expandableDiv : List (Html Msg) -> Html Msg
expandableDiv insides =
    label []
        [ input [ type_ "checkbox", class "click-overflow-check", onClickWithoutBubbling NoMsg ] []
        , div [ class "click-overflow-label", onClickWithoutBubbling NoMsg ] insides
        ]


expandableText : String -> Html Msg
expandableText string =
    if string == "" then
        expandableDiv [ text " " ]
    else
        expandableDiv [ text string ]


onClickWithoutBubbling : Msg -> Html.Attribute Msg
onClickWithoutBubbling msg =
    onWithOptions "click" { stopPropagation = True, preventDefault = False } (Decode.succeed msg)


linkBox : String -> List String -> Html Msg
linkBox root names =
    expandableDiv <| List.map (\name -> a [ href <| root ++ "/" ++ name ] [ text name ]) names


check : Bool -> Html Msg
check bool =
    if bool then
        div [ class "columns pl-20 green" ] [ icon "done" ]
    else
        div [ class "columns pl-20 silver" ] [ icon "done" ]


icon : String -> Html Msg
icon str =
    i [ class "material-icons pl-5 pr-5" ] [ text str ]


iconSmall : String -> Html Msg
iconSmall str =
    i [ class "material-icons pl-5 pr-5", style [ ( "font-size", "inherit" ) ] ] [ text str ]


hyperlink : String -> String -> Html Msg
hyperlink url linkText =
    button [ class "btn-link vertical-align-text" ] [ text linkText, icon "chevron_right" ]


listBox : List String -> Html Msg
listBox list =
    expandableText <| List.foldr (\old new -> old ++ "\n" ++ new) "" list
