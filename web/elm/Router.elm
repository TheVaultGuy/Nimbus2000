module Router exposing (..)

import ModelMsg exposing (..)
import RouteUrl exposing (..)
import RouteUrl.Builder exposing (..)
import Navigation exposing (Location)
import UrlParser exposing ((</>), (<?>))
import Model.Crud as Crud
import Rainfall.Searcher exposing (Filter(..))
import Dict


delta2url : Model -> Model -> Maybe UrlChange
delta2url oldModel newModel =
    let
        tableUrl =
            toString newModel.search.table

        viewIdUrl =
            case newModel.item of
                Just item ->
                    [ "TODO put viewId here" ]

                Nothing ->
                    []

        filterUrl =
            newModel.search.filters
                |> List.map
                    (\filter ->
                        case filter of
                            Column ( columnNum, keyword ) ->
                                "+col" ++ (toString columnNum) ++ keyword

                            Range ( columnNum, ( min, max ) ) ->
                                "+ran" ++ (toString columnNum) ++ min ++ "|" ++ max

                            Keyword keyword ->
                                "+key" ++ keyword
                    )
                |> String.concat
                |> (\x ->
                        if x /= "" then
                            [ ( "filters", x ) ]
                        else
                            []
                   )
                |> Dict.fromList
    in
        if oldModel.search.table /= newModel.search.table || oldModel.search.filters /= newModel.search.filters then
            Just (builder |> newEntry |> replacePath (tableUrl :: viewIdUrl) |> replaceQuery filterUrl |> toUrlChange)
        else
            Nothing



-- ( oldModel, newModel, builder |> newEntry )
--     |> assessPath
--     |> toUrl
--default URL = /tableName?filter=filter1+filter2&viewId=aswerasdfadsfGUID


location2messages : Location -> List Msg
location2messages location =
    let
        tableParser table =
            if (Debug.log ("Landing on Table ") table) == "" then
                [ SetUrl <| toUrlChange <| replacePath [ toString Crud.Projects ] <| modifyEntry <| RouteUrl.Builder.builder ]
            else if table == toString Crud.Projects then
                [ OnChangeTable Crud.Projects ]
            else if table == toString Crud.Drives then
                [ OnChangeTable Crud.Drives ]
            else if table == toString Crud.Media then
                [ OnChangeTable Crud.Media ]
            else if table == toString Crud.Users then
                [ OnChangeTable Crud.Users ]
            else if table == toString Crud.People then
                [ OnChangeTable Crud.People ]
            else if table == toString Crud.Scans then
                [ OnChangeTable Crud.Scans ]
            else if table == toString Crud.Media then
                [ OnChangeTable Crud.Media ]
            else if table == toString Crud.Transcriptions then
                [ OnChangeTable Crud.Transcriptions ]
            else if table == toString Crud.DIT_Reports then
                [ OnChangeTable Crud.DIT_Reports ]
            else if table == toString Crud.Assets then
                [ OnChangeTable Crud.Assets ]
            else if table == toString Crud.Purchases then
                [ OnChangeTable Crud.Purchases ]
            else if table == toString Crud.CC_Reports then
                [ OnChangeTable Crud.CC_Reports ]
            else
                [ On404 ]

        viewIdParser viewId =
            []

        filterParser filters =
            []
    in
        UrlParser.parsePath
            (UrlParser.map
                (\table viewId filters -> List.concat [ tableParser table, viewIdParser viewId, filterParser filters ])
             <|
                (UrlParser.string <?> UrlParser.stringParam "viewId" <?> UrlParser.stringParam "filter")
            )
            location
            |> Maybe.withDefault [ On404 ]
