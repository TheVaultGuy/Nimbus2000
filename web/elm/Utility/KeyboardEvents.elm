module Utility.KeyboardEvents exposing (..)

import Utility.Keys as Keys exposing (..)
import Keyboard
import Set exposing (Set)
import Task
import Debug


type alias KeyCombo =
    Set Keyboard.KeyCode


type alias KeyComboEvent =
    { key : Keyboard.KeyCode
    , combo : Set Keyboard.KeyCode
    }


type EventType
    = Down
    | Up


emptyCombo : Set Keyboard.KeyCode
emptyCombo =
    Set.empty


subscribe : Set Keyboard.KeyCode -> (KeyComboEvent -> msg) -> Sub msg
subscribe oldCombo outMsg =
    let
        toDownEvent downKey =
            { key = downKey, combo = Set.insert downKey oldCombo }

        toUpEvent upKey =
            { key = upKey, combo = Set.remove upKey oldCombo }

        downs =
            Keyboard.downs (outMsg << toDownEvent)

        ups =
            Keyboard.ups (outMsg << toUpEvent)
    in
        Sub.batch
            [ downs
            , ups
            ]


map : KeyComboEvent -> EventType -> List (Keys.Key) -> (a -> msg) -> a -> Cmd msg
map combo eventType target outMsg outParam =
    let
        targetSet =
            Set.fromList <| List.filterMap Keys.code target
    in
        if Set.member combo.key targetSet then
            case eventType of
                Down ->
                    if Set.member combo.key combo.combo && equals targetSet combo.combo then
                        Task.perform outMsg (Task.succeed outParam)
                    else
                        Cmd.none

                Up ->
                    if not (Set.member combo.key combo.combo) && equals targetSet (Set.insert combo.key combo.combo) then
                        Task.perform outMsg (Task.succeed outParam)
                    else
                        Cmd.none
        else
            Cmd.none


mapList : KeyComboEvent -> List ( EventType, List (Keys.Key), a -> msg, a ) -> Cmd msg
mapList combo eventList =
    let
        targetSetList =
            eventList |> List.map (\( a, b, c, d ) -> ( a, Set.fromList <| List.filterMap Keys.code b, c, d ))

        commandList =
            targetSetList
                |> List.map
                    (\( targetEvent, targetSet, outMsg, outParam ) ->
                        case targetEvent of
                            Down ->
                                if Set.member combo.key combo.combo && equals targetSet combo.combo then
                                    Task.perform outMsg (Task.succeed outParam)
                                else
                                    Cmd.none

                            Up ->
                                if not (Set.member combo.key combo.combo) && equals targetSet (Set.insert combo.key combo.combo) then
                                    Task.perform outMsg (Task.succeed outParam)
                                else
                                    Cmd.none
                    )
    in
        Cmd.batch commandList


equals : KeyCombo -> KeyCombo -> Bool
equals a b =
    let
        intersect =
            Set.intersect a b

        union =
            Set.union a b
    in
        Set.size intersect == Set.size union
