defmodule Nimbus2000.Drive do
  use Nimbus2000.Web, :model

  #@primary_key {:asset_id, :string, []}
  #@foreign_key {:asset_id, :string, []}
  @derive {Phoenix.Param, key: :asset_id}
  schema "drives" do
    field :description, :string
    field :name, :string
    field :short_name, :integer
    field :capacity, :integer
    field :transfer, :boolean

    belongs_to :asset, Nimbus2000.Project, foreign_key: :asset_id, references: :asset_tag
    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  @required_fields [:asset_id, :description, :capacity, :transfer]
  @optional_fields [:name, :short_name]
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> validate_length(:asset, min: 7, max: 7)
    |> unique_constraint(:asset_id)
    |> unique_constraint(:name, name: :drive_name_short_name_index)
  end
end
