defmodule Nimbus2000.Join.Drives_Media do
  use Nimbus2000.Web, :model

  @primary_key false
  schema "drives_media" do
    belongs_to :drives, Nimbus2000.HardDrives, primary_key: true
    belongs_to :media, Nimbus2000.Media, primary_key: true
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  @required_fields [:drives_id, :media_id]
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @required_fields)
    |> validate_required(@required_fields)
  end
end
