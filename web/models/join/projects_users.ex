defmodule Nimbus2000.Join.Projects_Users do
  use Nimbus2000.Web, :model

  @primary_key false
  schema "projects_users" do
    field :role, :string
    field :write, :boolean
    belongs_to :projects, Nimbus2000.Project, foreign_key: :project_id, references: :number, primary_key: true
    belongs_to :users, Nimbus2000.User, foreign_key: :user_id, references: :id, primary_key: true
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  @required_fields [:project_id, :user_id]
  @optional_fields [:role, :write]
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> unique_constraint(:unique_project_user, name: :unique_project_user_index)
  end
end
