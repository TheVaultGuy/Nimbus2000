defmodule Nimbus2000.Media do
  use Nimbus2000.Web, :model

  schema "media" do
    field :name, :string
    field :notes, :string
    field :offloadDate, Ecto.DateTime
    field :shootDate, Ecto.DateTime
    field :card, :string
    field :type, :string

    belongs_to :projects, Nimbus2000.Project, foreign_key: :project, references: :number
    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  @required_fields [:name, :project]
  @optional_fields [:notes, :offloadDate]
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
  end
end
