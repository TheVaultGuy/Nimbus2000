defmodule Nimbus2000.Person do
  use Nimbus2000.Web, :model

  schema "people" do
    field :first_name, :string
    field :last_name, :string
    field :company, :string
    field :bio, :string
    field :notes, :string
    field :profile_pic_url, :string

    has_one :user, Nimbus2000.User, foreign_key: :id
    many_to_many :projects, Nimbus2000.Project, join_through: "people_projects", join_keys: [person_id: :id, project_number: :number]
    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  @required_fields [:first_name, :last_name]
  @optional_fields [:company, :bio, :notes, :profile_pic_url]
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
  end
end
