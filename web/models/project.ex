defmodule Nimbus2000.Project do
  use Nimbus2000.Web, :model

  @primary_key {:number, :integer, []}
  @derive {Phoenix.Param, key: :number}
  schema "projects" do
    field :code, :string
    field :title, :string
    field :company, :string
    field :about, :string
    field :status, :string

    has_many :drives, Nimbus2000.HardDrive, references: :code, on_delete: :delete_all
    many_to_many :users, Nimbus2000.User, join_through: "projects_users", join_keys: [project_id: :number, user_id: :id]
    timestamps()
  end

  @required_fields [:number, :code, :title]
  @optional_fields [:company, :about, :status]
  def changeset(struct, params \\ %{}) do
    struct
    |> cast( params, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> unique_constraint(:number)
    |> unique_constraint(:code)
  end
end
