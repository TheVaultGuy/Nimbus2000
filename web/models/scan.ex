defmodule Nimbus2000.Scan do
  use Nimbus2000.Web, :model

  schema "scans" do
    field :location, :string
    field :owner, :string
    field :notes, :string
    belongs_to :projects, Nimbus2000.Project
    belongs_to :drives, Nimbus2000.HardDrive

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  @required_fields [:drive_id, :location]
  @optional_fields [:project_id, :owner, :notes]
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
  end
end
