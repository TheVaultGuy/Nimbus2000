defmodule Nimbus2000.User do
  use Nimbus2000.Web, :model
  alias Passport.Password

  #@primary_key {:person_id, :integer, []}
  #@foreign_key {:person_id, :integer, []}
  @derive {Poison.Encoder, except: [:__meta__], key: :id}
  schema "users" do
    field :first_name, :string, virtual: true
    field :last_name, :string, virtual: true
    field :email, :string
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true
    field :password_hash, :string
    field :admin, :boolean
    field :active, :boolean
    belongs_to :person, Nimbus2000.Person, foreign_key: :id, define_field: false, references: :id, primary_key: true
    many_to_many :projects, Nimbus2000.Project, join_through: "people_projects", join_keys: [person_id: :id, project_number: :number]
    # has_many :drives, through: [:projects, :drives], on_delete: :delete_all
    timestamps()
  end

@required_fields [:first_name, :last_name, :email, :password, :password_confirmation]
@optional_fields [:admin, :active]

  def changeset(model, params \\ %{}) do
    model
    |> cast(params, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> validate_length(:email, min: 1, max: 150)
    |> unique_constraint(:email)
    |> validate_length(:password, min: 6, max: 100)
    |> validate_confirmation(:password)
    |> put_hashed_password()
    |> check_person()
  end

  defp put_hashed_password(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pass, password_confirmation: pass}} ->
        put_change(changeset, :password_hash, Password.hash(pass))
        _ ->
          changeset
    end
  end

  defp check_person(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{first_name: first, last_name: last}} ->
        person = Nimbus2000.Repo.get_by(Nimbus2000.Person, first_name: first, last_name: last)
        if person && (Nimbus2000.Repo.get(Nimbus2000.User, person.id) == nil) do
          newChangeSet = changeset |> put_change(:id, person.id) |> unique_constraint(:id)
          case newChangeSet do
            %Ecto.Changeset{valid?: true}->
              newChangeSet
            _->
              changeset
              |> add_error(:first_name, "Person does not exist and cannot be added to the database", :last_name)
          end
        else
          personChangeset = Nimbus2000.Person.changeset(%Nimbus2000.Person{}, %{first_name: first, last_name: last})
          case Nimbus2000.Repo.insert(personChangeset) do
            {:ok, newPerson} ->
              changeset
              |> put_change(:id, newPerson.id)
              |> unique_constraint(:id)# Should not fail because the person was just created
            {:error, badPersonChanges} ->
              changeset
              |> add_error(:first_name, "Person does not exist and cannot be added to the database", :last_name)
          end
        end
      _ ->
        changeset
    end
  end

end
