defmodule Nimbus2000.Router do
  use Nimbus2000.Web, :router
  use Passport

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :assign_current_user
    plug :current_user
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Nimbus2000 do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/login", SessionController, :new
    post "/session", SessionController, :create
    get "/logout", SessionController, :delete
    get "/join", RegistrationController, :new
    post "/register", RegistrationController, :create
    get "/passwords/new", PasswordController, :new
    post "/passwords", PasswordController, :reset
    get "/404", PageController, :error404
    get "/*path", PageController, :index
  end

  # Other scopes may use custom stacks.
  scope "/api", Nimbus2000 do
    pipe_through :api

    resources "/drives", HardDriveController, except: [:new, :edit]
  end

  # Fetch the current user from the session and add it to `conn.assigns`. This
  # will allow you to have access to the current user in your views with
  # `@current_user`.
  defp assign_current_user(conn, _) do
   assign(conn, :current_user, get_session(conn, :current_user))
  end


end
