defmodule Nimbus2000.HardDriveView do
  use Nimbus2000.Web, :view

  def render("index.json", %{drives: drives}) do
    %{data: render_many(drives, Nimbus2000.HardDriveView, "hard_drive.json")}
  end

  def render("show.json", %{hard_drive: hard_drive}) do
    %{data: render_one(hard_drive, Nimbus2000.HardDriveView, "hard_drive.json")}
  end

  def render("hard_drive.json", %{hard_drive: hard_drive}) do
    %{id: hard_drive.id,
      assetTag: hard_drive.asset_tag,
      show: hard_drive.show,
      backupLetter: hard_drive.backup_letter,
      backupNumber: hard_drive.backup_number}
  end
end
